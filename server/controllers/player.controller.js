import Player from '../models/player';

export function getPlayers(req, res) {
    Player.find().exec((err, players) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ players });
    });
}

export function getPlayer(req, res) {
    Player.findOne({ cuid: req.params.cuid }).exec((err, players) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ players });
    });
}

// export function createPlayer(req, res) {
//     req.body.forEach(player => {
//         console.log('--- PLAYER --- ');
//         console.log(player);
//         console.log('--- PLAYER --- ');
//         if (player) {
//             let newPlayer = new Player({ ...player });
//             newPlayer.save()
//                 .then(resPlayer => {
//                     res.status(200).json({ resPlayer });
//                 })
//                 .catch(err => {
//                     res.status(500).send("unable to save to database");
//                 });

//         }
//     });
// }

export function createPlayer(req, res) {
    Player.insertMany(req.body)
        .then(resPlayer => {
            res.status(200).json([...resPlayer]);
        })
        .catch(err => {
            res.status(500).send("unable to save to database");
        });

}