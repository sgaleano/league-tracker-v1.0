import Match from '../models/match';

export function getMatches(req, res) {
    Match.find().exec((err, matches) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ matches });
    });
}

export function getMatch(req, res) {
    Match.findOne({ cuid: req.params.cuid }).exec((err, match) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ match });
    });
}