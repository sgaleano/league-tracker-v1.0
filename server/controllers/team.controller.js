import Team from '../models/team';

export function getTeams(req, res) {
    Team.find().exec((err, teams) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ teams });
    });
}

export function getTeam(req, res) {
    Team.findOne({ cuid: req.params.cuid }).exec((err, team) => {
        if (err) {
            res.status(500).send(err);
        }
        res.json({ team });
    });
}

export function createTeam(req, res) {
    const { name, players, logoId } = req.body
    let team = new Team({ name, players, logoId });
    team.save()
        .then(team => {
            res.status(200).json({team});
        })
        .catch(err => {
            res.status(500).send("unable to save to database");
        });
}