import { Router } from 'express';
import * as MatchController from '../controllers/match.controller';

const router = new Router();

router.route('/matches').get(MatchController.getMatches);

router.route('/matches/:cuid').get(MatchController.getMatch);

export default router;
