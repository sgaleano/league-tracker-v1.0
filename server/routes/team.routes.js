import { Router } from 'express';
import * as TeamController from '../controllers/team.controller';

const router = new Router();

router.route('/teams').get(TeamController.getTeams);

router.route('/teams/:cuid').get(TeamController.getTeam);

router.route('/teams').post(TeamController.createTeam);

export default router;
