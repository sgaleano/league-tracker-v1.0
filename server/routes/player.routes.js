import { Router } from 'express';
import * as PlayerController from '../controllers/player.controller';

const router = new Router();

router.route('/players').get(PlayerController.getPlayers);

router.route('/players/:cuid').get(PlayerController.getPlayer);

router.route('/players').post(PlayerController.createPlayer);

export default router;
