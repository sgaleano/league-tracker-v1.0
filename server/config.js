const config = {
  mongoURL: process.env.MONGO_URL || 'mongodb://admin:admin1234@ds211265.mlab.com:11265/trackyourteam',
  port: process.env.PORT || 8000,
};

export default config;
