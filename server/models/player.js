import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const postSchema = new Schema({
  playerName: { type: 'String', default: '' },
  age: { type: 'String'},
  isForeign: { type: 'Boolean', default: 0 },
  isGk: { type: 'Boolean', default: 0 },
  number: { type: 'String', default: 0 },
  team: { type: 'String' }
});

export default mongoose.model('Player', postSchema);
