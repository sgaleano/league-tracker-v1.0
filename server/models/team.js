import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const postSchema = new Schema({
  name: { type: 'String', required: true, default: '' },
  players: { type: 'Array', default: [] },
  wins: { type: 'Number', default: 0 },
  losses: { type: 'Number', default: 0 },
  draws: { type: 'Number', default: 0 },
  gf: { type: 'Number', default: 0 },
  gc: { type: 'Number', default: 0 },
  dateAdded: { type: 'Date', default: Date.now,},
  logoId: { type: 'String', required: true }
});

export default mongoose.model('Team', postSchema);
