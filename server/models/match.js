import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const matchSchema = new Schema({
    teamA: { type: 'String', required: true },
    teamB: { type: 'String', required: true },
    day: { type: 'String', required: true },
    location: { type: 'String', required: true },
    teamAScore: { type: 'Number'},
    teamBScore: { type: 'Number'},
    time: { type: 'String', requried: true },
    match: { type: 'Number', requried: true },
    dateAdded: { type: 'Date', default: Date.now, required: true },
});

export default mongoose.model('Match', matchSchema);