Simple app to keep track of a league of any type (Football, Basketball, Gaming, etc...)

This project is based in *React + Redux* and *Node.js + Express*
It has a MongoDB base in the cloud (mongolab)

*Steps to run the project*

1) Step into the root folder (Where is the _package.json_ file)
2) run in the console: npm install
3) run in the console: npm start
4) You should be able to see the APP in "http://localhost:8000"

*Objective*
The purpose of this project is to cover the basics of an application an its core functions (CRUD ie).
The main idea is to create an app that can be instantiated in any competitive environment to keep track of the state of a league, like how is the standing, which are the next matches, which are the most valuable players, which are the stats of the teams (goals, win matches, etc...) and so on.
It uses React JS in the Front End because of its Component Based paradigm which turns it into a very useful library in terms of reusing code, performance and easy development.
And the Back End is created with Node.js with Express, which are one of the top tecnologies nowadays to create lean, simple and fast REST API's. Its scafolded as a MVC project, having Controllers, Models, and Routes to send JSONS to the FE.

What it comes to Third Party Libraries, I've choosed the next ones:

    - FE:
        - React Router to manage the routing of the app, which has a rich documentation and community and is pretty easy to add to an existing project.
        - Redux to manage the general state of the app, I could used Context API but I found myself more comfortable with Redux and there is a bigger community and documentation for it.
        - Styled Components to keep the encapsulation concern about styling, trying to get rid of every CSS line in the app, this is almost not used in the project because I added it not so much ago, so some things still use simple CSS.
        - Axios to handle the HTTP requests, theres no much to say here, just a simple yet usefull library to keep my API calls clean and easy to use.
        - React Dropzone which I found very useful to update files/images, which is a feature that is often kinda hard to develop. It allows you to manage files upload through "Drag and drop" and also through simple "Click here" upload. Very handy
    
    - BE:
        - Mongoose, which is a framework for mongoDB to make easier the interacto with the DB.
        - Grid FS to handle file storing in the DB, pretty handy and easy to use and configure.
        

*SCAFOLDING*

I used a module based structure for the Front End of this project, represiting this way each feature of the applicaction as a *module*. 
For example, if you have a HOME feature in your app, you will see a HOME module in the structure, in which you will find it actions, reducer, containers, etc.
Also you will have a "components" folder in which you will find cross app components, within this directory you will put every component created that can be used everywhere and are not part of an specific feature.

Directory Example:

```
- src
    - components
        - Card
        - ItemList
        - Header
        - Footer
        - Content
    - modules
        - Home
            - components
                - Home.jsx
                - Form.jsx
            HomeContainer.jsx
            HomeActions.jsx
            HomeReducer.jsx
        - About
            - components
                - About.jsx
            AboutContainer.jsx
            AboutActions.jsx
            AboutReducer.jsx
    App.jsx
```


For the Back End side, the structure is very different, but with a very familiar pattern, which is the MVC, where you will find things like following:
    - Controller, this type of files will contain functions that are goin to be used in the Routers, this will have the implementation of the methods that are assigned to a specific route, using the provided functions from Mongoose API to interact with the DB.
    - Model, the boilerplate of every data model that you will have in the DB, this is how every byte of data will look like in the Data Base.
    - Routes, files that will handle every request and call an action assigned to that request. IE: /get-teams --> function getTeams() { // magic }
    
    
    
Directory Example:

```
- server
    - controllers
        - match.controller.js
        - team.controller.js
        - users.controller.js
    - models
        - match.js
        - team.js
        - user.js
    - routes
        - images.route.js
        - match.route.js
        - team.route.js
    config.js
    server.js
```




* Technology*

The use of ES6 in this project is one of the most important technologies of all, it provides very usefull functions which are the core of every JS project, despite of its framework.
Styled components will be replacing every line of CSS in the future.
CI workers will be added to keep a clean code and a good pracice in each feature.
Linterns are configured to keep the AirBNB coding style.
It has a babel polyfill configuration to keep the retrocompatibility with old browsers (IE11 for example)
It uses Webpack for the bundling and minification of every JS file, it also allows you to create a local environment to work in your machine, lifting a server to serve the static files.

This is a Project created by me, Santiago Galeano, following the MERN Starter pack boilerplate. It is enhanced (I hope) with my knowledge in the development environment.
