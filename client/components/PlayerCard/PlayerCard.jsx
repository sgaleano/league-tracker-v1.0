import React from 'react';

import styles from './PlayerCard.css';

const PlayerCard = (props) => {

    return (
        <div className={styles.playerCardWrapper} onClick={props.onClickHandler}>
            <div className={styles.playerCard}>
                <div className={styles.imgWrapper}>
                    <img className={styles.profilePicture} src={props.img || 'https://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640-300x300.png'} alt=""/>
                </div>
                <div className={styles.name}>
                    <span>{(props.player) ? props.player.playerName : '--'}</span>
                </div>
                <div className={styles.description}>
                    <div className={styles.descriptionContent}>
                        <span className={styles.info}><strong>Edad</strong> {(props.player) ? props.player.age : '--'}</span>
                        <span className={styles.info}><strong title="N° de Camiseta (Dorsal)">N°</strong> {(props.player) ? props.player.number : '--'}</span>
                    </div>
                    <div className={styles.descriptionContent}>
                        <span className={styles.info}><strong>Goles</strong> 0 </span>
                        <span className={styles.info}><strong title="Asistencias">Asist.</strong> 0</span>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default PlayerCard;