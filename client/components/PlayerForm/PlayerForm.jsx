import React, { Component } from 'react';

// Components
import { Modal, Button, ButtonGroup } from 'react-bootstrap';

class PlayerForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            playerName: '',
            number: '',
            age: '',
            isGk: '',
            isForeign: ''
        };
    }

    componentWillMount() {
        this.setState({
            playerName: this.props.player.playerName || '',
            number: this.props.player.number || '',
            age: this.props.player.age || '',
            isGk: this.props.player.isGk || '',
            isForeign: this.props.player.isForeign || '',
        });
    }

    handleOnChange = (e, key) => {
        const val = e.target.value;
        setTimeout(() => {
            this.setState({
                [key]: val
            });
        }, 0);
    }

    submitHandler = () => {
        this.props.createPlayer(this.state, this.props.index);
        setTimeout(() => {
            this.props.hideModal();
        }, 0);
    }

    render() {
        const { showModal, hideModal, player } = this.props;
        const { playerName, number, age, isGk, isForeign } = this.state;
        return (
            <Modal show={showModal} onHide={hideModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Agregar Jugador</Modal.Title>
                </Modal.Header>
                <form action="">
                    <Modal.Body>
                        
                        <div className="form-group">
                            <label htmlFor="playerName">Nombre y Apellido</label>
                            <input
                                onChange={e => this.handleOnChange(e, 'playerName')}
                                className="form-control"
                                type="text"
                                name="playerName"
                                value={playerName} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="number">N° de Camiseta (Dorsal)</label>
                            <input 
                                onChange={e => this.handleOnChange(e, 'number')}
                                className="form-control"
                                type="number"
                                name="number"
                                value={number} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="age">Edad</label>
                            <input 
                                onChange={e => this.handleOnChange(e, 'age')}
                                className="form-control"
                                type="text"
                                name="age"
                                value={age} />
                        </div>

                        <div className="form-group">
                            <label htmlFor="isGk">Es arquero?</label>
                            <ButtonGroup>
                                <Button className={isGk ? 'btn btn-success' : 'btn btn-dark'} onClick={() => this.setState({isGk:true})}>Si</Button>
                                <Button className={isGk ? 'btn btn-dark' : 'btn btn-success'} onClick={() => this.setState({isGk:false})}>No</Button>
                            </ButtonGroup>
                        </div>

                        <div className="form-group">
                            <label htmlFor="isForeign">Es extranjero?</label>
                            <ButtonGroup>
                                <Button className={isForeign ? 'btn btn-success' : 'btn btn-dark'} onClick={() => this.setState({isForeign:true})}>Si</Button>
                                <Button className={isForeign ? 'btn btn-dark' : 'btn btn-success'} onClick={() => this.setState({isForeign:false})}>No</Button>
                            </ButtonGroup>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={hideModal}>
                            Cancelar
                        </Button>
                        <Button variant="primary" onClick={this.submitHandler}>
                            Guardar
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>

        )
    }

}

export default PlayerForm;