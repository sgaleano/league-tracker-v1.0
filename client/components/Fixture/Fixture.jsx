import React from 'react';
import styles from './Fixture.css';

// Styled Components
import StyledCard from '../../styled-components/styledCard';

const Fixture = ({ matches }) => {

    const handleMatches = (matchDay) => {};

    return (
        <StyledCard>
            <div className={styles.header}>Fixture</div>
            <div className="d-flex align-items-center justify-content-between">
                <span className={styles.changeMatch} onClick={ () => handleMatches(-1)}>
                    <i className="fas fa-caret-left"></i>
                </span>
                <div className={styles.matches}>
                    <div className={styles.match}>
                        <div className={styles.team}><span className={styles.teamName}>Team A</span><img className={styles.teamLogo} src="http://www.promiedos.com.ar/images/s30/bocajuniors.png" alt=""/></div>
                        <div className={styles.score}> 3 : 0 </div>
                        <div className={styles.team}><img className={styles.teamLogo} src="http://www.promiedos.com.ar/images/s30/riverplate.png" alt=""/><span className={styles.teamName}>Team B</span></div>
                    </div>
                    <div className={styles.match}>
                        <div className={styles.team}><span className={styles.teamName}>Team A</span><img className={styles.teamLogo} src="http://www.promiedos.com.ar/images/s30/bocajuniors.png" alt=""/></div>
                        <div className={styles.score}> 3 : 0 </div>
                        <div className={styles.team}><img className={styles.teamLogo} src="http://www.promiedos.com.ar/images/s30/riverplate.png" alt=""/><span className={styles.teamName}>Team B</span></div>
                    </div>
                </div>
                <span className={styles.changeMatch} onClick={ () => handleMatches(1)}>
                    <i className="fas fa-caret-right"></i>
                </span>
            </div>
        </StyledCard>
    )

}

export default Fixture;