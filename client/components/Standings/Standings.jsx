import React from 'react';
import { Link } from 'react-router';

import styles from './Standings.css';

// Styled Components
import StyledCard from '../../styled-components/styledCard';

const Standings = ({ teams }) => {
    return (
        <StyledCard>
            <div className={styles.headerTitle}>Tabla de Posiciones</div>
            <div className="Standings">
                <div className={styles.teamItem + ' ' + styles.header}>
                    <div className={styles.twoUnits} title="Posición">Pos.</div>
                    <div className={styles.team} title="Equipo">Equipo</div>
                    <div className={styles.twoUnits} title="Puntos">Pts.</div>
                    <div className={styles.oneUnit} title="Partidos ganados">G</div>
                    <div className={styles.oneUnit} title="Partidos perdidos">P</div>
                    <div className={styles.oneUnit} title="Partidos empatados">E</div>
                    <div className={styles.oneUnit} title="Goles a favor">GF</div>
                    <div className={styles.oneUnit} title="Goles en contra">GC</div>
                    <div className={styles.oneUnit} title="Diferencia de goles">DG</div>
                </div>
                {teams.map((team, index) => {
                    return (
                        <div key={team._id} className={styles.teamItem}>
                            <div className={styles.twoUnits}>{index + 1}</div>
                            <div className={styles.team}><Link to={`teams/${team.name}`} >{team.name}</Link></div>
                            <div className={styles.twoUnits}>{team.pts || 32}</div>
                            <div className={styles.oneUnit}>{team.w || 3}</div>
                            <div className={styles.oneUnit}>{team.l}</div>
                            <div className={styles.oneUnit}>{team.d}</div>
                            <div className={styles.oneUnit}>{team.gf}</div>
                            <div className={styles.oneUnit}>{team.gc}</div>
                            <div className={styles.oneUnit}>{team.dg}</div>
                        </div>
                    );
                })}
            </div>
        </StyledCard>
    );
};

export default Standings;
