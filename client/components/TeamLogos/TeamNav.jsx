import React from 'react';

import styles from './TeamNav.css';

import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { Link } from 'react-router';

const TeamNav = ({ teams }) => {

    return (
        <div className={styles.teamList}>
            {teams.map((team) => {
                return (
                    <OverlayTrigger
                        key="top"
                        placement="top"
                        overlay={
                            <Tooltip>
                                Boca Juniors
                            </Tooltip>
                        }
                    >
                        <Link to="sadads">
                            <img src="http://www.promiedos.com.ar/images/s30/bocajuniors.png" alt="" />
                        </Link>
                    </OverlayTrigger>

                )
            })}
        </div>
    )

}

export default TeamNav;