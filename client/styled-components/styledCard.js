import styled from 'styled-components';

const StyledCard = styled.div`
    border: 4px solid #20c1eb;
    margin-bottom: 10px;
`;

export default StyledCard;
