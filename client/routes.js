/* eslint-disable global-require */
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './modules/App/App';

import Teams from './modules/Team/pages/TeamListPage';
import HomePage from './modules/Home/HomePage';

// Admin Section Pages
import AdminHome from './modules/Admin/AdminHome';
import AdminTeam from './modules/Admin/AdminTeam';
import AddTeam from './modules/Forms/TeamForm';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route exact path="/teams" component={Teams} />
    <Route exact path="/teams/:teamName" component={Teams} />
    <Route exact path="/admin" component={AdminHome} />
    <Route exact path="/admin/equipos" component={AdminTeam} />
    <Route exact path="/admin/agregar-equipo" component={AddTeam} />
  </Route>
);
