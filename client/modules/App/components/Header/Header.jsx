import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

// Import Style
import styles from './Header.css';

const Header = () => {

  return (
    <nav className={'navbar fixed-top justify-content-between ' + styles.Header}>
      <img src="" alt=""/>
      <ul className={styles.links}>
        <li className={styles.linkItem}><Link to="/" >Inicio</Link></li>
      </ul>
    </nav>
  )
  
}

Header.contextTypes = {
  router: PropTypes.object,
};

Header.propTypes = {
  toggleAddPost: PropTypes.func.isRequired,
  switchLanguage: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
};

export default Header;
