import { connect } from 'react-redux';

// Actions
import * as actions from '../Team/TeamActions';

// Components
import Home from './components/Home';

const mapStateToProps = (state) => ({
    teams: state.team.teams,
    matches: state.team.matches
});

const mapDispatchToProps = dispatch => ({
    fetchTeams: (data) => dispatch(actions.fetchTeams(data)),
    fetchMatches: (data) => dispatch(actions.fetchMatches(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);