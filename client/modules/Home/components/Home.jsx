import React, { Component } from 'react';

// Components
import Standings from '../../../components/Standings/Standings';
import Fixture from '../../../components/Fixture/Fixture';
import { Link } from 'react-router';
import { Button } from 'react-bootstrap';

class Home extends Component {

    componentDidMount() {
        this.props.fetchTeams();
        this.props.fetchMatches();
    }

    render() {
        const { teams, matches } = this.props;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-8">
                        <Standings teams={teams} />
                        <Fixture matches={matches} />
                    </div>
                    <div className="col-md-4">
                        <Link to="admin/agregar-equipo" >
                            <Button className="btn btn-success">Agregar nuevo equipo</Button>
                        </Link>
                    </div>
                </div>

            </div>
        );
    }

}

export default Home;