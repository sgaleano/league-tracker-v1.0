import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import Standings from '../../../components/Standings/Standings';
import Fixture from '../../../components/Fixture/Fixture';

class TeamList extends Component {

    componentDidMount() {
        this.props.fetchTeams();
        this.props.fetchMatches();
    }

    render() {
        const { teams, matches } = this.props;
        return(
            <div>
                <Standings teams={teams} />
                <Fixture matches={matches} />
            </div>
        );
    }

}

export default TeamList;