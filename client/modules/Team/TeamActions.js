import { axiosApi } from '../../util/apiCaller';
import axios from 'axios';

export function getTeams(value) {
    return {
        type: 'GET_TEAMS',
        payload: value
    };
}

export function postTeam(value) {
    return {
        type: 'POST_TEAM',
        payload: value
    };
}

export function getMatches(value) {
    return {
        type: 'GET_MATCHES',
        payload: value
    };
}

export function selectPlayer(index) {
    return {
        type: 'SELECT_PLAYER',
        payload: index
    }
}

export function postPlayer(player, index) {
    const payload = { player, index }
    return {
        type: 'POST_PLAYER',
        payload
    }
}

export function fetchTeams() {
    return async (dispatch) => {
        let response = {}
        try {
            response = await axiosApi('teams');
        } catch (e) {
            console.log(e);
        }
        dispatch(getTeams(response.data.teams));
    }
}

export function fetchMatches() {
    return async (dispatch) => {
        let response = {}
        try {
            response = await axiosApi('matches');
        } catch (e) {
            console.log(e);
        }
        dispatch(getMatches(response.data.matches));
    }
}

async function saveImage(team) {
    const response = await axiosApi('uploadimage', 'POST', team, { 'content-type': 'multipart/form-data' });
    return response.data;
}

async function storePlayers(players) {
    let response = {}
    try {
        response = await axiosApi('players', 'POST', players);
    } catch (e) {
        console.log(e);
    }
    return response;
}

export function createTeam(img, team) {
    return async (dispatch) => {
        const imgResponse = await saveImage(img);
        const storedPlayers = await storePlayers(team.players);
        team.logoId  = imgResponse.file.id;
        team.players = [storedPlayers];
        let response = {}
        try {
            response = await axiosApi('teams', 'POST', team);
        } catch (e) {
            console.log(e);
        }
        dispatch(postTeam(response));
    }
}

export function createPlayer(player, index) {
    return async (dispatch) => {
        // const imgResponse = await saveImage(img);
        // team.logoId  = imgResponse.file.id;
        // let response = {}
        // try {
        //     response = await axiosApi('players', 'POST', player);
        // } catch (e) {
        //     console.log(e);
        // }
        // dispatch(postPlayer(response.data.player, index));
        dispatch(postPlayer(player, index));
    }
}

export function setTeamImage(imageData) {
    return {
        type: 'SET_IMAGE',
        payload: imageData
    };
}