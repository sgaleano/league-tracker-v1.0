const initialState = {
    teams: [],
    matches: [],
    team: { players: [] },
    player: {},
    image: ''
};

export default function teamReducer(state = initialState, action) {
    switch (action.type) {
        
        case 'GET_TEAMS': {
            return {
                ...state,
                teams: action.payload
            };
        }

        case 'POST_TEAM': {
            return {
                ...state,
                team: action.payload.data.team,
            };
        }

        case 'GET_MATCHES': {
            return {
                ...state,
                matches: action.payload
            };
        }

        case 'SELECT_PLAYER': {
            const player = (state.team.players[action.payload]) ? state.team.players[action.payload] : {};
            return {
                ...state,
                player
            };
        }

        case 'POST_PLAYER': {
            let team = {...state.team};
            team.players[action.payload.index] = action.payload.player;
            return {
                ...state,
                player: action.payload.player,
                team
            };
        }

        case 'SET_IMAGE': {
            return {
                ...state,
                image: action.payload
            };
        }
            
        default:
            return {...state};
    }
}