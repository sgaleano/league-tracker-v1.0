import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Dropzone from 'react-dropzone'

import {
    createTeam,
    selectPlayer,
    createPlayer,
    setTeamImage
} from '../Team/TeamActions';

// Components
import PlayerCard from '../../components/PlayerCard/PlayerCard';
import PlayerForm from '../../components/PlayerForm/PlayerForm';

import styles from './TeamForm.css';

// Styled Components
const PlayersList = styled.div`
    text-align: center;
    margin-top: 25px;
`;

class TeamForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showPlayers: false,
            showModal: false,
            index: 0,
            file: {},
            mountPlayerForm: false,
        }
    }

    submitHandler = (e) => {
        e.preventDefault();
        let imgFormData = new FormData();
        imgFormData.append('file', this.state.image);
        this.props.team.name = e.target.teamName.value;
        this.props.createTeam(imgFormData, this.props.team);
    }

    onClickHandler = (index) => {
        this.props.selectPlayer(index);
        this.setState({ showModal: true, mountPlayerForm: true, index });
    }

    hideModal = () => {
        this.setState({ showModal: false, mountPlayerForm: false });
    }

    onDrop = (acceptedFiles, rejectedFiles) => {
        this.props.setTeamImage(URL.createObjectURL(acceptedFiles[0]));
        this.setState({ image: acceptedFiles[0] });
    }

    render() {
        const { showModal, index, mountPlayerForm } = this.state;
        const { image, player, team, createPlayer } = this.props;

        return (
            <div>
                {mountPlayerForm &&
                    <PlayerForm
                        player={player}
                        hideModal={this.hideModal}
                        showModal={showModal}
                        index={index}
                        createPlayer={createPlayer}
                    />
                }
                <Dropzone onDrop={this.onDrop}>
                    {({ getRootProps, getInputProps, isDragActive }) => {
                        return (
                            <div {...getRootProps()} className={styles.defaultTeam} style={{ backgroundImage: `url(${image})` }}>
                                <input {...getInputProps()} />
                                {!isDragActive ?
                                    <div className={styles.dropDesc}>
                                        <p>Arrastrá y soltá la imagen del logo del club acá.</p>
                                        <p>ó hacé click para elegir una imagen de tu computadora.</p>
                                    </div>
                                    :
                                    <div className={styles.activeWrapper}>
                                        <span className={styles.dropDesc + ' ' + styles.active}>Soltá la imagen acá.</span>
                                    </div>
                                }
                            </div>
                        )
                    }}
                </Dropzone>
                <form onSubmit={this.submitHandler} className={styles.TeamForm}>
                    <input className={styles.teamName} type="text" required placeholder="Nombre del Equipo" name="teamName" />
                    <PlayersList>
                        <PlayerCard player={team.players[0]} onClickHandler={() => this.onClickHandler(0)} />
                        <PlayerCard player={team.players[1]} onClickHandler={() => this.onClickHandler(1)} />
                        {/* <PlayerCard player={team.players[2]} onClickHandler={() => this.onClickHandler(2)} />
                            <PlayerCard player={team.players[3]} onClickHandler={() => this.onClickHandler(3)} />
                            <PlayerCard player={team.players[4]} onClickHandler={() => this.onClickHandler(4)} />
                            <PlayerCard player={team.players[5]} onClickHandler={() => this.onClickHandler(5)} />
                            <PlayerCard player={team.players[6]} onClickHandler={() => this.onClickHandler(6)} />
                            <PlayerCard player={team.players[7]} onClickHandler={() => this.onClickHandler(7)} />
                            <PlayerCard player={team.players[8]} onClickHandler={() => this.onClickHandler(8)} />
                            <PlayerCard player={team.players[9]} onClickHandler={() => this.onClickHandler(9)} />
                            <PlayerCard player={team.players[10]} onClickHandler={() => this.onClickHandler(10)} /> */}
                    </PlayersList>
                    <input className={"btn btn-success " + styles.submitTeamName} type="submit" value="siguiente" />
                </form>
            </div>
        )
    }

}

const mapStateToProps = state => ({
    team: state.team.team,
    player: state.team.player,
    image: state.team.image
});

const mapDispatchToProps = dispatch => ({
    createTeam: (imgFormData, team) => dispatch(createTeam(imgFormData, team)),
    selectPlayer: (index) => dispatch(selectPlayer(index)),
    createPlayer: (player, index) => dispatch(createPlayer(player, index)),
    setTeamImage: (image) => dispatch(setTeamImage(image))
});

export default connect(mapStateToProps, mapDispatchToProps)(TeamForm);