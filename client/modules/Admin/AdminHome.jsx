import React from 'react';
import { Link } from 'react-router';

const AdminHome = () => {

    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <Link to="admin/equipos" >administrar equipos</Link>
                    <button className="btn btn-primary">
                        Administrar Jugadores
                    </button>
                    <button className="btn btn-primary">
                        Administrar Torneos
                    </button>
                </div>
            </div>
        </div>
    );

}

export default AdminHome;