import React, { Component } from 'react';

// Components
import { Link } from 'react-router';

class AdminTeam extends Component {

    componentDidMount() {

    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-sm-12">
                        <Link to="admin/agregar-equipo" >Agregar equipo</Link>
                    </div>
                    <div className="col-sm-12">
                        Listado de equipos
                        <Link to="admin/editar-equipo/id" >admin</Link>
                    </div>
                </div>
            </div>
        )
    }

}

export default AdminTeam;